\documentclass{fancyhandout}

\usepackage{outlines}
\usepackage[yyyymmdd,hhmmss]{datetime}
\usepackage{float}
\usepackage{amsmath,amsfonts,amssymb,commath,mathtools,physics}
\usepackage{mdframed,tcolorbox}
\usepackage{booktabs}
\usepackage{tikz,pgfplots}
\usepgflibrary{shapes.geometric}
\usetikzlibrary{calc}
\pgfplotsset{my style/.append style={axis x line=middle, axis y line=middle, xlabel={$x$}, ylabel={$y$}, axis equal}}

\usepackage{array}



\tcbuselibrary{theorems}
\newtcbtheorem[number within=section]{exercise}{Exercise}
{}{}

\usepackage[colorlinks=true]{hyperref}

\everymath{\displaystyle}

\title{Recitation 2}
\author{Winston Cheong}
\date{Last Update: \today \ \currenttime}

\colorlet{fancyhandouttboxlinecolor}{white}
\colorlet{fancyhandouttboxfillcolor}{white}

\begin{document}
\maketitle
%\tableofcontents

\section{\S2.1. A Preview of Calculus}
\subsection{Overall Idea}
Let $s(t)$ denote the distance of some object at time $t$. 
The \emph{average velocity} of the object from time $t = a$ to $t = b$ can be computed as 
\[
	v_{avg} = \frac{s(b) - s(a)}{b-a}
\]
If we consider shrinking the time interval arbitrarily small, this calculation will give us the \emph{instantaneous velocity}:
\[
	v_{instant} = \lim_{\Delta t \to 0} \frac{s(a + \Delta t) - s(a)}{\Delta t}
\]

This is the motivation behind the notion of \emph{derivative}, which shows up in the next chapter.

\subsection{Practice}

\begin{exercise*}{2.4--2.6. Like 2.1--2.3}
	Let $P = (1, 2)$, $Q = (x, y)$ be points on $f(x) = x^3$.
	Let $m$ be the slope of the secant line $\overline{PQ}$.
	
	\begin{itemize}
		\item 
			Complete the table (use 8 significant digits):
			\begin{table}[H]
				\centering
				\begin{tabular}{m{1in}m{1in}m{1in}}
					\toprule
					$x$ & $y$ & $m$\\
					\midrule
					1.1 & & \\
					1.01 & & \\
					1.001 & & \\
					1.0001 & & \\
					\bottomrule
				\end{tabular}
			\end{table}
		\item 
			Guess the value of the slope of the tangent line to $f$ at $x = 1$.\\
		\item 
			Find the equation of the tangent line at point $P$. \\
		\item 
			Graph $f(x)$ and the tangent line.
			\vskip 1in
			~
	\end{itemize}
\end{exercise*}

\begin{exercise*}{Like 2.18--2.19}
	Consider a stone tossed into the air from ground level (of some arbitrary planet) with an initial velocity of 20 m/sec. 
	Its height in meters at time $t$ seeconds is $h(t) = 20t - 10t^2$.

	Compute the average velocity of the stone over the given time intervals:
	\begin{enumerate}
		\item $[1, 1.05]$ \\ 
		\item $[1, 1.01]$ \\ 
		\item $[1, 1.005]$ \\ 
		\item $[1, 1.001]$ \\ 
	\end{enumerate}

	Use this to guess the instantaneous velocity of the stone at $t = 1$ sec.
\end{exercise*}


\newpage
\section{\S2.2. The Limit of a Function}

$\lim_{x\to a^-}$ approaches from the left.\\

$\lim_{x\to a^+}$ approaches from the right.\\

$\lim_{x\to a} f(x) = L \iff \lim_{x\to a^-} f(x) = L = \lim_{x\to a^+} f(x)$

\subsection{Practice}

\begin{exercise*}{Like 2.30--2.31}
	Consider the function $f(x) = \frac{x^3-1}{|x-1|}$.
	Complete the following table (rounding to 4 decimal places).
	
	\begin{table}[H]
		\centering
		\begin{tabular}{rc}
			\toprule
			$x$ & $\qquad f(x)\qquad$ \\
			\midrule
			0.9\\
			0.99\\
			0.999\\
			0.9999\\
			\midrule
			1.0001\\
			1.001\\
			1.01\\
			1.1\\
			\bottomrule
		\end{tabular}
	\end{table}
\end{exercise*}

\begin{exercise*}{Like 2.36}
	Set up the table to evaluate the limit $\lim_{x\to 0} \frac{\sin 3x}{x}$.
	Round your values to 8 decimal places.
	
	\begin{table}[H]
		\centering
		\begin{tabular}{rc}
			\toprule
			$x$ & $\qquad\frac{\sin 3x}{x}\qquad$\\
			\midrule
			$-0.1$ & \\
			$-0.01$ & \\
			$-0.001$ & \\
			$-0.0001$ & \\
			\midrule
			$0.0001$\\
			$0.001$\\
			$0.01$\\
			$0.1$\\
			\bottomrule
		\end{tabular}
	\end{table}

	$\lim_{x\to0} \frac{\sin3x}{x} = $
\end{exercise*}

\begin{exercise*}{Like 2.44}
	Fill in the table of values (round to 8 significant digits). 
	\begin{table}[H]
		\centering
		\begin{tabular}{rc}
			\toprule
			$\theta$ & $\qquad\cos(\frac{\pi}{\theta})\qquad$\\
			\midrule
			$-0.1$ & \\
			$-0.01$ & \\
			$-0.001$ & \\
			$-0.0001$ & \\
			\midrule
			$0.0001$\\
			$0.001$\\
			$0.01$\\
			$0.1$\\
			\bottomrule
		\end{tabular}
	\end{table}

	Based on this table, guess: $\lim_{\theta\to0} \cos(\frac{\pi}{\theta}) = $ 
	
	\vskip1em~
	
	Now, use a graphing calculator to graph the function and determine the limit. 

	Was your guess correct? If not, why does the method of tables fail?
	\vskip1em~
\end{exercise*}

\begin{exercise*}{Like 2.46--2.49}
	Consider the following graph for a function $f(x)$. Determine which statements are true and which are false. 
	If false, explain why.

	\begin{center}
		\begin{tikzpicture}
			\begin{axis}[my style, minor tick num=4]
				\addplot[domain=-11:-5.1] {1/(x+5)};
				\addplot[domain=-4.9:0] {-1/(x+5)};
				\addplot[domain=0:2] {x^3+2};
				\addplot[domain=2:10] {abs(x-7)-7};

				\addplot[mark=*,only marks] coordinates {(-3,4)(0,-3)(2,10)};
				\addplot[mark=*,fill=white,only marks] coordinates {(-3,-.5)(0,-.2)(0,2)(2,-2)};
			\end{axis}
		\end{tikzpicture}
	\end{center}

	\begin{enumerate}
		\item $\lim_{x\to0} f(x)$ exists
		\item $\lim_{x\to-3} f(x) = f(-3)$
		\item $\lim_{x\to2^-} f(x) = -2$
		\item $\lim_{x\to2} f(x) = 10$
	\end{enumerate}
\end{exercise*}

\begin{exercise*}{Like 2.50--2.54}
	Use the graph of $f(x)$ below to find the values, if possible. Estimate when necessary.
	\begin{center}
		\begin{tikzpicture}
			\begin{axis}[my style]
				\addplot[domain=-5:0] {-x+1};
				\addplot[domain=0:3] {x^2-4};
				\addplot[mark=*,only marks] coordinates {(0,-4)};
				\addplot[mark=*,fill=white,only marks] coordinates {(0,1)};
			\end{axis}
		\end{tikzpicture}
	\end{center}
	\begin{enumerate}
		\item $\lim_{x\to0^-}f(x)$
		\item $\lim_{x\to0^+}f(x)$
		\item $\lim_{x\to0}f(x)$
		\item $f(0)$
		\item $\lim_{x\to2}f(x)$
	\end{enumerate}
\end{exercise*}

\end{document}
